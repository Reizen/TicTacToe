#include "Board.h"

using namespace std;

Board::Board()
{
	memset(_boardSlots, 0, sizeof(int) * MAX_SLOTS * MAX_SLOTS);
}

void Board::reset()
{
	memset(_boardSlots, 0, sizeof(int) * MAX_SLOTS * MAX_SLOTS);
}

void Board::draw()
{
	cout << "\n     1   2   3 \n";
	cout << "    --- --- ---\n";
	for (int x = 0; x < MAX_SLOTS; x++)
	{
		cout << " " << x + 1 << " | ";

		for (int y = 0; y < MAX_SLOTS; y++)
		{
			if (_boardSlots[x][y] == 0)
				cout << " " << " | ";
			else if (_boardSlots[x][y] == 1)
				cout << "X" << " | ";
			else
				cout << "O" << " | ";
		}

		cout << "\n    --- --- ---\n";
	}

	cout << endl;
}

bool Board::markMove(int x, int y, int playerMarker)
{
    bool isValidMove = true;
    
    if ((x < 0 || x > MAX_SLOTS) ||
        (y < 0 || y > MAX_SLOTS))
    {
        isValidMove = false;
    }
    else if (_boardSlots[x - 1][y - 1] != 0)
    {
        isValidMove = false;
    }
    
    if (isValidMove)
        _boardSlots[x - 1][y - 1] = playerMarker;
    
    return isValidMove;
}

int Board::isWinFound()
{
	int winnerNo = 0;
	int curSlotCount = 0;
	int curPlayer = 1;

	for (curPlayer; curPlayer <= 2; curPlayer++)
	{
		// Check horizontals
		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				if (_boardSlots[x][y] == curPlayer)
				{
					curSlotCount++;
				}
				else
				{
					curSlotCount = 0;
					break;
				}
			}

			if (curSlotCount == 3) { winnerNo = curPlayer; break; }
			curSlotCount = 0;
		}

		// Check verticals
		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				if (_boardSlots[y][x] == curPlayer)
				{
					curSlotCount++;
				}
				else
				{
					curSlotCount = 0;
					break;
				}
			}

			if (curSlotCount == 3) { winnerNo = curPlayer; break; }
			curSlotCount = 0;
		}

		// Check Diagonals
		// 3,1 2,2 1,3
		// 1,1 2,2 3,3
		if ((_boardSlots[0][0] == curPlayer && _boardSlots[1][1] == curPlayer && _boardSlots[2][2] == curPlayer) ||
			(_boardSlots[2][0] == curPlayer && _boardSlots[1][1] == curPlayer && _boardSlots[0][2] == curPlayer))
		{
			winnerNo = curPlayer;
			break;
		}
	}

	return winnerNo;
}

bool Board::isDrawFound()
{
	bool isDraw = true;

	for (int x = 0; x < 3; x++)
	{
		for (int y = 0; y < 3; y++)
		{
			if (_boardSlots[x][y] == 0)
			{
				isDraw = false;
				break;
			}
		}
	}

	return isDraw;
}

Board::~Board()
{
}
