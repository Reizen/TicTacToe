#include "Player.h"

Player::Player()
{
	_score = 0;
}

void Player::upScore()
{
	_score++;
}

void Player::resetScore()
{
	_score = 0;
}

int Player::getScore()
{
	return _score;
}

void Player::setSlot(int i)
{
	_slot = i;
}

int Player::getSlot()
{
    return _slot;
}

char Player::slotToC()
{
	return (_slot == 1) ? 'X' : 'O';
}

Player::~Player()
{
}
