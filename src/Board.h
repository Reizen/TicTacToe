#ifndef _BOARD_H_
#define _BOARD_H_

#include <iostream>
#include <cstring>

class Board
{
public:
	static const int MAX_SLOTS = 3;

	Board();
	void reset();
	void draw();
	bool markMove(int, int, int);
	int isWinFound();
	bool isDrawFound();
	~Board();

private:
	int _boardSlots[MAX_SLOTS][MAX_SLOTS];
};

#endif