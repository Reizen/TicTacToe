#include "Game.h"

using namespace std;

Game::Game()
{
	init();
}

void Game::init()
{
	_playerOne = new Player();
	_playerOne->setSlot(1);
	_playerTwo = new Player();
	_playerTwo->setSlot(2);

	_hud = new HUD(_playerOne, _playerTwo);
	_board = new Board();

	_curPlayer = 1;
}

void Game::run()
{
	_running = true;

    while (_running)
    {
        _hud->draw();
        _board->draw();
        getPlayerMove();
        Util::clr();
		_hud->draw();
		_board->draw();
		checkWin();
		Util::clr();
    }
}

void Game::getPlayerMove()
{
	int x, y;
    bool isValid = false;

	cout << "Player " << _curPlayer << " turn!\n";

	do
	{
		cout << "Enter move X: ";
		cin >> x;
		cout << "Enter move Y: ";
		cin >> y;

		if (_board->markMove(x, y, _curPlayer))
		{
			isValid = true;
			break;
		}
		else
		{
			isValid = false;
			cout << "Invalid input!\n";
		}

	} while (!isValid);
    
    switchActivePlayer();
}

void Game::switchActivePlayer()
{
	(_curPlayer == 1) ? _curPlayer = 2 : _curPlayer = 1;
}

void Game::checkWin()
{
	int win = _board->isWinFound();
	int action = 0;

	if (win != 0)
	{
		if (win == 1)
		{
			_playerOne->upScore();
		}
		else
		{
			_playerOne->upScore();
		}

		cout << "Player one won!\n";
		cout << "Press 1 to play again 0 to exit: ";
		cin >> action;

		if (action == 0)
			this->_running = false;
		else
			_board->reset();
	}

	if (_board->isDrawFound())
	{
		cout << "Game is draw!\n";
		cout << "Press 1 to play again 0 to exit: ";
		cin >> action;

		_board->reset();
	}
}

Game::~Game()
{
	delete _hud;
	delete _board;
	delete _playerOne;
	delete _playerTwo;
}
