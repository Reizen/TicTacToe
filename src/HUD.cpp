#include "HUD.h"

using namespace std;

HUD::HUD(Player * playerOne, Player * playerTwo)
{
	this->_playerOne = playerOne;
	this->_playerTwo = playerTwo;
}

void HUD::draw()
{
	cout << "Player 1 (" << _playerOne->slotToC() << "): " << _playerOne->getScore() << endl;
	cout << "Player 2 (" << _playerTwo->slotToC() << "): " << _playerTwo->getScore() << endl;
}

HUD::~HUD()
{	
}