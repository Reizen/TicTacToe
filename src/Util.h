#ifndef _UTIL_H_
#define _UTIL_H_

#include <iostream>

class Util
{
public:
	static void pause();
	static void clr();
};

#endif