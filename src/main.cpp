#include <iostream>
#include "Game.h"
#include "Util.h"

int main()
{
    Util::clr();
    
	Game * _game = new Game();
	_game->run();

	delete _game;
    
	Util::pause();

	return 0;
}