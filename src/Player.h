#ifndef _PLAYER_H_
#define _PLAYER_H_

class Player
{
public:
	Player();

	void upScore();
	void resetScore();
	int getScore();
	void setSlot(int);
    int getSlot();
	char slotToC();

	~Player();

private:
	int _slot;
	int _score;
};

#endif