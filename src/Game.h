#ifndef _GAME_H_
#define _GAME_H_

#include <iostream>
#include "HUD.h"
#include "Board.h"
#include "Util.h"

class Game
{
public:
	Game();
	void run();
	~Game();

private:
	void init();
	void getPlayerMove();
    void switchActivePlayer();
	void checkWin();

private:
	HUD * _hud;
	Board * _board;
	Player * _playerOne;
	Player * _playerTwo;
	bool _running;
	int _curPlayer;
};

#endif