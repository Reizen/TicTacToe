#ifndef _HUD_H_
#define _HUD_H_

#include "Player.h"
#include <iostream>

class HUD
{
public:
	HUD(Player *, Player *);
	void draw();
	~HUD();

private:
	Player * _playerOne;
	Player * _playerTwo;
};

#endif