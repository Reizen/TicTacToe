#include "Util.h"

using namespace std;

void Util::pause()
{
#ifdef _WIN32
	system("PAUSE");
#endif
}

void Util::clr()
{
#ifdef _WIN32
	system("cls");
#else
	system("clear");
#endif
}